package com.odelia.microprofile.reactive

import org.apache.camel.component.file.GenericFile
import org.eclipse.microprofile.reactive.messaging.Incoming
import org.eclipse.microprofile.reactive.messaging.Outgoing
import org.eclipse.microprofile.reactive.streams.operators.ReactiveStreams
import org.reactivestreams.Publisher
import org.slf4j.LoggerFactory
import java.io.File
import javax.enterprise.context.ApplicationScoped


@ApplicationScoped
open class CamelReactiveWithConfig {
    private val logger = LoggerFactory.getLogger(CamelReactiveWithConfig::class.java)

    // Use channel camel1 defined in microprofile-config.properties
    @Incoming("camel1")
    @Outgoing("text1")
    open fun consume(genfile: GenericFile<File>) : Publisher<String> {
        return ReactiveStreams.fromIterable(genfile.file.readLines()).buildRs()
    }

    @Incoming("text1")
    open fun handleText(text: String) : Unit {
        logger.info("Text: {}", text)
    }
}