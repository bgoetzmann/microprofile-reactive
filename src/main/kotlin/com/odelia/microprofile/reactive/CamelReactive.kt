package com.odelia.microprofile.reactive

import org.eclipse.microprofile.reactive.messaging.Incoming
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import org.apache.camel.component.reactive.streams.api.CamelReactiveStreamsService
import org.apache.camel.Exchange
import org.reactivestreams.Publisher
import org.eclipse.microprofile.reactive.messaging.Outgoing
import org.eclipse.microprofile.reactive.streams.operators.ReactiveStreams
import org.slf4j.LoggerFactory
import java.io.File


@ApplicationScoped
open class CamelReactive {
    private val logger = LoggerFactory.getLogger(CamelReactive::class.java)

    @Inject
    private lateinit var camelReactive: CamelReactiveStreamsService

    @Outgoing("camel")
    open fun source(): Publisher<Exchange> {
        return camelReactive.from("file:///temp/camel?delay=1000")
    }

    @Incoming("camel")
    @Outgoing("text")
    open fun consume(exchange: Exchange) : Publisher<String> {
        val file = exchange.`in`.getBody(File::class.java)
        return ReactiveStreams.fromIterable(file.readLines()).buildRs()
    }

    @Incoming("text")
    open fun handleText(text: String) : Unit {
        logger.info("Text: {}", text)
    }
}