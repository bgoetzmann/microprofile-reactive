# microprofile-reactive

microprofile-reactive is a MicroProfile application demonstrating MicroProfile Reactive Messaging API with SmallRye implementation.  
Please, see [this](https://www.odelia-technologies.com/blog/mp-reactive-messaging.html) corresponding article. 

This project was tested with WildFly 17.

In particular, you can use these Gradle tasks:

- `war` to build the war file.
- `deploy` to deploy the application in WildFly 17 (see `build.gradle.kts` for details).

## CamelReactive class

This Kotlin class uses a Camel route defined in its `source` method; it detects file in `/temp/camel` directory.

## CamelReactiveWithConfig class

This Kotlin class uses a Camel route defined in `microprofile-config.properties`; this route detects files in `/temp/camel1` directory.  