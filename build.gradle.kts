import com.mkring.wildlydeplyplugin.DeployWildflyTask

plugins {
    // Apply the Kotlin JVM plugin to add support for Kotlin on the JVM.
    id("org.jetbrains.kotlin.jvm").version("1.3.21")
    id("com.mkring.wildlydeplyplugin.deploy-wildfly-plugin").version("0.2.12")
    id("war")
}

group = "com.odelia"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    // Use the Kotlin JDK 8 standard library.
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    compileOnly("javax:javaee-api:8.0")
    compileOnly("org.eclipse.microprofile:microprofile:2.2")

    compile("io.smallrye.reactive:smallrye-reactive-messaging-provider-1.0:1.0.0")
    compile("io.smallrye.reactive:smallrye-reactive-streams-operators-1.0:1.0.6")
    compile("io.smallrye.reactive:smallrye-reactive-messaging-camel-1.0:1.0.0")
    compile("org.apache.camel:camel-cdi:3.0.0-M3")
    compile("io.smallrye:smallrye-config-1.3:1.0.0")

    compile("org.slf4j:slf4j-api:1.7.26")
    compile("ch.qos.logback:logback-core:1.2.3")
    compile("ch.qos.logback:logback-classic:1.2.3")

    // Use the Kotlin test library.
    testImplementation("org.jetbrains.kotlin:kotlin-test")

    // Use the Kotlin JUnit integration.
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

task("deploy", DeployWildflyTask::class) {
    host = "localhost"
    port = 9990
    user = "admin"
    password = "adminadmin"
    deploymentName = project.name                //cli: --name=$runtimeName
    runtimeName = "${project.name}-$version.war" //cli: --runtime-name=$runtimeName
    // filepath, here a war example
    file = "$buildDir/libs/${project.name}-$version.war".apply { println("file=$this") }
}
